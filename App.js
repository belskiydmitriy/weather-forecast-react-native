import React from 'react';
import { StyleSheet, Content, Text, View, ImageBackground, Image } from 'react-native';
import Title from './src/Title';
import { useFonts, Ubuntu_400Regular, Ubuntu_700Bold } from '@expo-google-fonts/ubuntu';
import { colors } from './src/utils/constant';
import ArrowIcon from './src/components/ArrowIcon';
import WeekForecast from './src/components/Weekforecast';

export default function App() {
  let [loaded] = useFonts({
    Ubuntu_400Regular, Ubuntu_700Bold
  });
  
  if (!loaded) {
    return null;
  } else {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require('./src/images/background/Bg-up.png')}
          style={styles.imageTop}
          resizeMode='stretch'
        >
          <ImageBackground
            source={require('./src/images/background/Bg-bottom.png')}
            style={styles.imageBottom}
            resizeMode='center'
          >
            <View style={styles.wrapper}>
              <Title />
              <WeekForecast />
              <ArrowIcon opened={false}/>
            </View>
          </ImageBackground>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.accent,
  },
  imageTop: {
    flex: 1,
    width: '100%',
    height: '50%',
  },
  imageBottom: {
    flex: 1,
    width: '100%',
    height: '160%',
  },
  wrapper: {
    flex: 1,
    alignItems: 'center',
  },
});
