import React from 'react';
import { StyleSheet, Image } from 'react-native';
import images from '../../utils/images';

export default function ArrowIcon({ opened }) {
  return (
    <Image source={opened ? images.arrowTop : images.arrowBottom} style={styles.arrowIcon} />
  );
}

const styles = StyleSheet.create({
  arrowIcon: {
    height: 20,
    width: 20,
  }
});