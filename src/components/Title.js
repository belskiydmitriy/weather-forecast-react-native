import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { vw } from 'react-native-expo-viewport-units';

function Title() {
  return (
    <View style={styles.title}>
      <View style={styles.titleTop}><Text style={styles.titleFont}>Weather</Text></View>
      <View style={styles.titleBottom}><Text style={styles.titleFont}>forecast</Text></View>
    </View>
  );
}

export default Title;

const styles = StyleSheet.create({
  title: {
    flex: 0,
    flexDirection: "column",
    width: vw(70),
    paddingVertical: vw(7.5),
    paddingHorizontal: 0,
    marginVertical: 0,
    marginHorizontal: 'auto',
    paddingVertical: 40,
  },
  titleTop: {
    alignItems: 'flex-start',
  },
  titleBottom: {
    alignItems: 'flex-end',
  },
  titleFont: {
    fontSize: vw(9.96),
    lineHeight: vw(9.96),
    fontFamily: 'Ubuntu_700Bold',
    fontStyle: 'normal',
    fontWeight: 'bold',
    color: '#ffffff',
  }
});
