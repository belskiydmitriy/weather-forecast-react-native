import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Placeholder from '../placeholder/Placeholder';
import SelectCity from '../selectCity/SelectCity';
import Slider from '../slider/Slider';
import { apiKey, cities } from '../../utils/constant';
import { vw } from 'react-native-expo-viewport-units';
import { colors } from '../../utils/constant';

export default function WeekForecast() {
  const [cityNames, setCityNames] = useState([]);
  const [selectedCity, setSelectedCity] = useState('');
  const [weeklyResponse, setWeeklyResponse] = useState();
  const [windowWidth, setWindowWidth] = useState(0);
  const [rangeValue, setRangeValue] = useState(0);
  const [weekForecast, setWeeklyForecast] = useState([]);

  const handleResize = () => {
    setWindowWidth(window.innerWidth);
  };

  useEffect(() => {
    const citiesArray = cities.map((city) => (
      { id: city.id, value: city.name }
    ));
    setCityNames(citiesArray);

    // setWindowWidth(window.innerWidth);
    // window.addEventListener('resize', handleResize);
    // return () => {
    //   window.removeEventListener('resize', handleResize);
    // };
  }, []);

  const handleSliderChange = (e) => {
    setRangeValue(parseInt(e.target.value, 10));
  };

  useEffect(() => {
    const city = cities.find((cityItem) => cityItem.name === selectedCity);
    if (city) {
      fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${city.latitude}&lon=${city.longitude}&units=metric&exclude=alerts,current,minutely,hourly&appid=${apiKey}`)
        .then((response) => response.json())
        .then((json) => setWeeklyResponse(json));
    }
  }, [selectedCity]);

  function getCardDataFromRawDailyForecast() {
    if (weeklyResponse) {
      return weeklyResponse.daily.map((rawDay) => {
        const unixTimestamp = rawDay.dt;
        const date = new Date(unixTimestamp * 1000);
        return {
          day: date.getUTCDate(),
          month: date.getUTCMonth() + 1,
          year: date.getUTCFullYear(),
          temperature: rawDay.temp.day,
          icon: rawDay.weather[0].icon,
        };
      });
    }
    return undefined;
  }

  useEffect(() => {
    setWeeklyForecast(getCardDataFromRawDailyForecast);
  }, [weeklyResponse]);

  return (
    <View style={styles.container}>
      <Text style={styles.h1Font}>7 Days Forecast</Text>
      <View style={styles.selectwrapper}>
        {/* <Selectcity setCity={setSelectedCity} cities={cityNames} /> */}
      </View>
      {/* { weekForecast?.length ? <Slider windowWidth={windowWidth} rangeValue={rangeValue} cardsContent={weekForecast} cardAmount={windowWidth <= 768 ? 7 : 3} /> : <Placeholder message="" /> } */}
      {/* { weekForecast?.length ? <input onChange={handleSliderChange} className="input-range" type="range" /> : <></> } */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 500,
    width: '86%',
    paddingTop: vw(6), // 19
    paddingLeft: vw(7.4),
    paddingRight: vw(7.4),
    paddingBottom: vw(2.2), // 53
    marginTop: 0,
    marginRight: 15,
    marginBottom: 30,
    marginLeft: 15,
    position: 'relative',
    backgroundColor: colors.baseWeak,
    // boxShadow: 0px 4px 4px rgba(4, 5, 73, 0.25), 14px 14px 20px rgba(5, 6, 114, 0.2),
    shadowColor: "rgba(4, 5, 73, 0.25)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,
    elevation: 8,
    borderRadius: 8,
    overflow: 'hidden',
  },
  containerTop: {
    maxHeight: 524,
    minHeight: vw(13.4), // 421
  },
  selectwrapper: {
    // flex-wrap: wrap,
    marginBottom: vw(7.5), // 24
    justifyContent: 'space-between',
  },
  h1Font: {
    fontFamily: 'Ubuntu_700Bold',
    fontSize: 32,
    lineHeight: 32,
    fontWeight: 'bold',
    marginBottom: 32,
    color: colors.baseStrong,
  }
});




