const images = {
  arrowTop: require('../images/icons/16/chevron-top.png'),
  arrowBottom: require('../images/icons/16/chevron-bottom.png'),
}
export default images;