export const colors = {
  accent: '#373af5',
  secondary: '#8083a4',
  secondary60: 'rgba(128, 131, 164, 0.6)',
  secondary40: 'rgba(128, 131, 164, 0.4)',
  secondary20: 'rgba(128, 131, 164, 0.2)',
  secondary06: 'rgba(128, 131, 164, 0.06)',
  baseStrong: '#2c2d76',
  critic: '#e5596d',
  baseWeak: '#ffffff',
};

export const apiKey = '40e04da9c5aabaa94f2c9c89aeeb4578';

export const cities = [
  {
    id: 1, name: 'Самара', latitude: 53.195873, longitude: 50.100193,
  },
  {
    id: 2, name: 'Тольятти', latitude: 53.507836, longitude: 49.420393,
  },
  {
    id: 3, name: 'Саратов', latitude: 51.533557, longitude: 46.034257,
  },
  {
    id: 4, name: 'Казань', latitude: 55.796127, longitude: 49.106405,
  },
  {
    id: 5, name: 'Краснодар', latitude: 45.035470, longitude: 38.975313,
  },
];

export const firstYear = 1970;

export const months = [
  { id: 1, value: 'Jan' },
  { id: 2, value: 'Feb' },
  { id: 3, value: 'Mar' },
  { id: 4, value: 'Apr' },
  { id: 5, value: 'May' },
  { id: 6, value: 'Jun' },
  { id: 7, value: 'Jul' },
  { id: 8, value: 'Aug' },
  { id: 9, value: 'Sept' },
  { id: 10, value: 'Oct' },
  { id: 11, value: 'Nov' },
  { id: 12, value: 'Dec' },
];

export const monthsDays = [
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
];
